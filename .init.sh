#!/usr/bin/env bash
ldir "CONFIG" "${XDG_CONFIG_HOME:-"$HOME/.config"}"
ldir_bin "BIN" "$HOME/.local/bin"
ldir "LIB" "$HOME/.local/lib"
